All phpRAD code is Copyright 2015 by Eduardo Martos (emartos@natiboo.es).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Dugong includes works under other copyright notices and distributed
according to the terms of the GNU General Public License or a compatible
license, including:

  Predis - Copyright (c) 2009-2014 Daniele Alessandri
  Smarty - Copyright (c) 2001-2005 New Digital Group, Inc.
  Bootstrap - Copyright (c) 2015 Twitter