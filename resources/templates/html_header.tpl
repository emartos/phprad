<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8 />
    <title>{{ title }}</title>
    {% for colKey,file in css %}
    <link href="{{ file }}" rel="stylesheet" type="text/css" />
    {% endfor %}
    {% for colKey,file in js %}
    <script src="{{ file }}"></script>
    {% endfor %}
  </head>
  <body>
