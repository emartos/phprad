<p>This is the content of a sample Redis database:</p>
<table id="scriptlist" class="table dataTable table-striped table-bordered" cellspacing="0" width="100%">
    {% for rowKey,row in data %}
    <tr>
        {% for colKey,col in row %}
        <td>{{ col }}</td>
        {% endfor %}
    </tr>
    {% endfor %}
</table>
{% if messages is not empty %}
{{ messages }}
{% endif %}
