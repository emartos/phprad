<h2>CSV</h2>
<div>
{% autoescape %}
{{ csv_action|raw }}
{% endautoescape %}
</div>
<h2>MySQL</h2>
<div>
{% autoescape %}
{{ mysql_action|raw }}
{% endautoescape %}
</div>
<h2>Redis</h2>
<div>
{% autoescape %}
{{ redis_action|raw }}
{% endautoescape %}
</div>
{% if messages is not empty %}
{{ messages }}
{% endif %}