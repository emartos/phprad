<p>Send a CSV file</p>
<form action="?bundle=Bundle" method="post" enctype="multipart/form-data">
  <label for="csv">CSV file</label>&nbsp;<input type="file" name="csv" accept=".csv" />
  <input type="submit" value="Send" />
</form>
{% if messages is not empty %}
{{ messages}}
{% endif %}
