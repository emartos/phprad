
This is the parsed content of a sample CSV file:

{% set rowResult = '' %}
{% for rowKey,row in csv %}
{% for colKey,col in row %}
{% set rowResult = rowResult ~ col ~ "\t\t" %}
{% endfor %}
{{ rowResult }}
{% set rowResult = '' %}
{% endfor %}

{% if messages is not empty %}
{{ messages }}
{% endif %}

