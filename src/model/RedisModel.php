<?php
/**
 * RedisModel.class.php
 * Model class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class RedisModel extends BaseModel implements IModel {
    /**
     * Class constructor
     */
    public function __construct() {
        $this->initializeDb('redis');
    }

    /**
     * Load method
     * @param string $params
     * @return mixed|void
     */
    public function load($params = '') {
        $result = '';
        $query = 'sales:' . $params['year'] . ':' . $params['month'];
        $db = $this->getDbHandler('redis');
        $dbHandler = new $db();
        try {
            $result = $dbHandler->query($query);
        } catch (DbConnectionException $e) {
            Error::fatal();
        }
        if ($result) {
          $this->assign('data', $result);;
        }
    }
}