<?php
/**
 * CsvModel.class.php
 * Model class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class CsvModel extends BaseModel implements IModel {
    /**
     * Class constructor
     */
    public function __construct() {}

    /**
     * Load method
     * @param string $params
     * @return string|void
     */
    public function load($params = '') {
      if (File::exists($params['file'])) {
        $csv = array_map('str_getcsv', file($params['file']));
        foreach ($csv as &$row) {
          $row[0] = explode($params['delimiter'], $row[0]);
        }
        $ret = $csv;
        $this->assign('csv', $ret);
      }
    }
}
