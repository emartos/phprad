<?php
/**
 * ExtendedModel.class.php
 * Extended model allows you to add common functionality to your models
 * You need to instantiate this class where you are planning to use it
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class ExtendedModel extends BaseModel {
}