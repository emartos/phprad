<?php
/**
 * DailyBrowserController.class.php
 * Bundle class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class DailyBrowserController extends BaseController implements IController {
	/**
	 * Class constructor
	 * @param $controllerId
	 * @param bool $silent
	 */
	public function __construct($controllerId, $silent = false) {
		parent::__construct($controllerId, $silent);
		$this->title = 'Bundle action';
		// Set renderers (HTML must be the first renderer)
		$this->renderers = array(HTML_RENDERER, PDF_RENDERER);
	}

	/**
	 * Execute the action
	 * @return string
	 * @throws ParametersException
	 * @throws RendererException
	 */
	public function execute() {
		$ret = '';
		try {
			// Check errors
			$this->check();
			// Execute the actions
			$ret['csv_action'] =$this->executeBundledAction('Csv');
			$ret['mysql_action'] =$this->executeBundledAction('Mysql');
			$ret['redis_action'] =$this->executeBundledAction('Redis');
		} catch (ParametersException $e) {
			throw new ParametersException();
		} catch (RendererException $e) {
			throw new RendererException($e->getMessage());
		}
		return $ret;
	}

	/**
	 * Check if the action is executable
	 * @throws ParametersException
	 */
	private function check() {
		$extendedController = new ExtendedController();
		// Check parameters
		$params = Parameters::getInstance();
		$csv = $params->get('csv');
		$syntax[CLI] = 'php -f index.php bundle=<bundle id> csv=<csv path>';
		$syntax[HTTP] = 'Missing csv parameter';
		if (!$extendedController->checkParams((bool)$csv, $syntax[APP_SAPI])) {
			throw new ParametersException();
		}
	}
}

