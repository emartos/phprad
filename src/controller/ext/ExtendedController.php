<?php
/**
 * ExtendedController.class.php
 * Extended controller allows you to add common functionality to your controllers
 * You need to instantiate this class where you are planning to use it
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class ExtendedController {
    /**
     * Check if the controller has received the right parameters
     * @param $condition
     * @param $syntax
     * @return bool
     */
    public function checkParams($condition, $syntax) {
        if (!$condition) {
            $message = 'WRONG SYNTAX: The right syntax is: ' . $syntax;
            $messageBus = MessageBus::getInstance();
            $messageBus->put($message, _LOG_FATAL, true);
            $ret = false;
        } else {
            $ret = true;
        }
       return $ret;
    }
}