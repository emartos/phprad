<?php
/**
 * MysqlController.class.php
 * Action class - Sample controller that shows the content of a MySQL database
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class MysqlController extends BaseController implements IController {
	/**
	 * Class constructor
	 * @param $controllerId
	 * @param bool $silent
	 */
	public function __construct($controllerId, $silent = false) {
		parent::__construct($controllerId, $silent);
		$this->type = C_CONTROLLER;
		$this->title = 'MySQL action';
		// Set renderers (HTML must be the first renderer)
		$this->renderers = array(HTML_RENDERER, PDF_RENDERER);
	}

	/**
	 * Execute the action
	 * @return mixed
	 * @throws ParametersException
	 */
	public function execute() {
		try {
			// Check errors
			$this->check();
			// Parse the file
			$ret['data'] = $this->getData();
		} catch (ParametersException $e) {
			throw new ParametersException();
		}
		return $ret;
	}

	/**
	 * Check if the action is executable
	 * @return boolean
	 */
	private function check() {}

	/**
	* Retrieve the content of the database
	*
	* @return array
	*/
	private function getData() {
		$this->model->load();
		$data = $this->model->get('data');
		// Refine the data
		if ($data) {
			foreach ($data as &$row) {
				foreach ($row as &$col) {
					$monthName = Date::getMonthName($col);
					if (is_numeric($col)) {
						$col = number_format($col);
					}
					if (!empty($monthName)) {
						$col = $monthName;
					}
				}
			}
		}
		return $data;
	}
}