<?php
/**
 * BundleFormController.class.php
 * Action class - Sample controller that shows a form tu submit a CSV file
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class BundleFormController extends BaseController implements IController {
	/**
	 * Class constructor
	 * @param $controllerId
	 * @param bool $silent
	 */
	public function __construct($controllerId, $silent = false) {
		parent::__construct($controllerId, $silent);
		$this->type = C_BUNDLE;
		$this->title = 'Bundle form';
		// Set renderers (HTML must be the first renderer)
		$this->renderers = array(HTML_RENDERER);
	}

	/**
	 * Execute the action
	 * @return string
	 */
	public function execute() {}

	/**
	 * Check if the action is executable
	 * @return boolean
	 */
	private function check() {}
}