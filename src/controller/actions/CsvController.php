<?php
/**
 * CsvController.class.php
 * Action class - Sample controller that parses a CSV file
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class CsvController extends BaseController implements IController {
    /**
     * Class constructor
     * @param $controllerId
     * @param bool $silent
     */
    public function __construct($controllerId, $silent = false) {
        parent::__construct($controllerId, $silent);
                    $this->type = C_CONTROLLER;
        $this->title = 'CSV action';
        // Set renderers (HTML must be the first renderer)
        $this->renderers = array(HTML_RENDERER, PDF_RENDERER);
    }

    /**
     * Execute the action
     * @return mixed
     * @throws ParametersException
     */
    public function execute() {
        try {
            // Check errors
            $this->check();
            // Parse the file
            $ret['csv'] = $this->parseCsv();
        } catch (ParametersException $e) {
            throw new ParametersException($e->getMessage());
        }
        return $ret;
    }

    /**
     * Check if the action is executable
     * @throws ParametersException
     */
    private function check() {
        $params = Parameters::getInstance();
        $extendedController = new ExtendedController();
        // Check parameters
        $csv = $params->get('csv');
        $syntax[CLI] = 'php -f index.php action=[action id] csv=[csv path]';
        $syntax[HTTP] = 'Missing csv parameter';
        if (!$extendedController->checkParams((bool)$csv, $syntax[APP_SAPI])) {
            $message = 'Required parameter missing: ' . $syntax[APP_SAPI];
            throw new ParametersException($message);
        }
        $this->args['csv'] = $csv;
    }

    /**
     * Parse the CSV file
     * @return array
     */
    private function parseCsv() {
        $params = array('file' => $this->args['csv'], 'delimiter' => ';');
        $this->model->load($params);
        $data = $this->model->get('csv');
        // Refine the CSV
        if ($data) {
            foreach ($data as &$row) {
                $row = $row[0];
                foreach ($row as &$col) {
                    $monthName = Date::getMonthName($col);
                    if (is_numeric($col)) {
                        $col = number_format($col);
                    }
                    if (!empty($monthName)) {
                        $col = $monthName;
                    }
                }
            }
        }
        return $data;
    }
}