
WHAT'S NEXT
------------

phpRAD has a good architecture: simple, flexible and clear. But there are some improvements I am working on to make a better phpRAD:

 * Doctrine integration
 * AngularJS integration
 * Minification
 * PHP cache
 * Filters system
 * Better security (XSS, SQL injections...)
 * Full scaffolding with CRUD (model, view and controller)

CONTRIBUTORS
------------

This is an open source project now and forever. If you would like to contribute, write an email to emartos@natiboo.es. Help is always appreciated!
