<?php
/**
 * RendererException.class.php
 * Class to handle exceptions related to renderers
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class RendererException extends Exception {
	/**
	 * Class constructor
	 * @param string $message
	 * @param int $code
	 * @param Exception|null $previous
	 */
	public function __construct($message = '', $code = 0, Exception $previous = null) {
		if (empty($message)) {
			$message = 'Render error';
		}
		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}" . NL;
	}
}