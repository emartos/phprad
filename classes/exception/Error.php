<?php
/**
 * Error.class.php
 * Error handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Error {
	/**
	 * Manage a warning error
	 * @param string $message
	 */
	public static function warning($message = '') {
		// Print the message
		print self::renderMessage($message);
	}

	/**
	 * Manage a fatal error
	 * @param string $message
	 */
	public static function fatal($message = '') {
		// Print the message
		print self::renderMessage($message);
		// Finish execution
		die;
	}

	/**
	 * Render an error message
	 * @param string $message
	 * @return string
	 */
	private static function renderMessage($message = '') {
		// Get the application messages
		if (empty($message)) {
			$messageBus = MessageBus::getInstance();
			$message = $messageBus->getMessages();
		}
		// Render the template
		try {
			$renderer = new HtmlRenderer('error', TPL);
			$output = $renderer->render(array('content' => $message));
		} catch (RendererException $e) {
			die($e->getMessage());
		}
		return $output;
	}
}