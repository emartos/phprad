<?php
/**
 * Date.class.php
 * Date helper
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Date {
	/**
	 * Set the timezone
	 * @param $timezone
	 */
	public static function setTimezone($timezone) {
		date_default_timezone_set($timezone);
	}

	/**
	 * Return the current date in a file compatible format
	 * @return string
	 */
	public static function now() {
		return date('d-m-Y_H-i');
	}

	/**
	 * Add an interval to a date
	 * @param $date
	 * @param $interval
	 * @return mixed
	 */
	public static function add($date, $interval) {
		$date->add(new DateInterval($interval));
		return $date;
	}

	/**
	 * Check what kind of day is date based on options
	 * options is an array containing weekdays correspondence and with a special key called 'holiday'
	 * @param $date
	 * @param $options
	 * @param string $holidays
	 * @return mixed
	 */
	public static function checkDate($date, $options, $holidays = '') {
		$weekday = date('N', strtotime($date));
		if ($holidays && in_array($date, $holidays)) {
			$ret = $options['holiday'];
		} else {
			$ret = $options[$weekday];
		}
		return $ret;
	}

	/**
	 * Get the difference between two dates in days
	 * @param $start
	 * @param $end
	 * @return string
	 */
	public static function getInterval($start, $end) {
		$datetime1 = new DateTime($start);
		$datetime2 = new DateTime($end);
		$interval = $datetime1->diff($datetime2);
		return $interval->format('%a');
	}

	/**
	 * Get weeks in an interval
	 * @param $start
	 * @param $end
	 * @param $options
	 * @param $weekendStart
	 * @return array
	 */
	public static function getWeekends($start, $end, $options, $weekendStart) {
		$start = new DateTime($start);
		$end = new DateTime($end);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$days = array();
		foreach ($period as $dt) {
			if ($dt->format('N') == $weekendStart) {
				$days[$dt->format('Y-m-d')] = $options[$weekendStart];
				// Add sunday
				$dt->add(new DateInterval('P1D'));
				$days[$dt->format('Y-m-d')] = $options[$weekendStart + 1];
			}
		}
		return $days;
	}

	/**
	 * Get mondays in an interval
	 * @param $start
	 * @param $end
	 * @param $options
	 * @param $weekStart
	 * @return mixed
	 */
	public static function getMondays($start, $end, $options, $weekStart) {
		$days = 0;
		$start = new DateTime($start);
		$end = new DateTime($end);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		foreach ($period as $dt) {
			if ($dt->format('N') == $weekStart) {
				$days[$dt->format('Y-m-d')] = $options[$weekStart];
			}
		}
		return $days;
	}

	/**
	 * Get holidays in an interval
	 * @param $start
	 * @param $end
	 * @param $options
	 * @param $holidays
	 * @return array
	 */
	public static function getHolidays($start, $end, $options, $holidays) {
		$start = new DateTime($start);
		$end = new DateTime($end);
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$days = array();
		foreach ($period as $dt) {
			if (in_array($dt->format('Y-m-d'), $holidays)) {
				$days[$dt->format('Y-m-d')] = $options['holiday'];
			}
		}
		return $days;
	}

	/**
	 * Get last day of a month
	 * @param $date
	 * @return bool|string
	 */
	public static function getLastDayOfMonth($date) {
		return date('t', strtotime($date));
	}

	/**
	 * Check if a day is the first of a month
	 * @param $date
	 * @return bool
	 */
	public static function isFirstDayOfMonth($date) {
		$day = date('d', strtotime($date));
		return ($day == 1);
	}

	/**
	 * Check if a day is the last of a month
	 * @param $date
	 * @return bool
	 */
	public static function isLastDayOfMonth($date) {
		$day = date('d', strtotime($date));
		$daysOfMonth = self::getLastDayOfMonth($date);
		return ($day == $daysOfMonth);
	}

	/**
	 * Check if a day is the first or last of a month
	 * @param $date
	 * @return bool
	 */
	public static function isFirstOrLastDayOfMonth($date) {
		return self::isFirstDayOfMonth($date) || self::isFirstDayOfMonth($date);
	}

	/**
	 * Get the elapsed time between two timestamps in seconds
	 * @param $start
	 * @param $end
	 * @return string
	 */
	public static function getElapsedTime($start, $end) {
		$seconds = ((int)$end - (int)$start);
		return gmdate('H:i:s', (int)$seconds);
	}

	/**
	 * Get the month name from the month number
	 * @param $m
	 * @return string
	 */
	public static function getMonthName($m) {
	  $dateObj = DateTime::createFromFormat('!m', $m);
	  return is_object($dateObj) ? $dateObj->format('F') : '';
	}
}