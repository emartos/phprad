<?php
/**
 * Curl.class.php
 * Curl class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Curl {
    /**
     * Send data
     * @param $url
     * @param $data
     */
    public static function send($url, $data) {
        // url-ify the data
        $count = count($data);
        $dataString = '';
        foreach ($data as $key => $value) {
            $dataString .= $key.'='.$value.'&';
        }
        rtrim($dataString, '&');
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, $count);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $dataString);
        // Execute call
        curl_exec($ch);
        // Close connection
        curl_close($ch);
    }
}