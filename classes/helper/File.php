<?php
/**
 * File.class.php
 * File helper
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class File {
    /**
     * Check if a file exists
     * @param $filename
     * @return bool
     */
    public static function exists($filename) {
        return file_exists($filename);
    }

    /**
     * Read a file
     * @param $filename
     * @return bool|string
     */
    public static function read($filename) {
        if (!file_exists($filename)) {
            touch($filename);
            $ret = false;
        } else {
            $ret = file_get_contents($filename);
        }
        return $ret;
    }

    /**
     * Write a file
     * @param $filename
     * @param $content
     * @return bool|int
     */
    public static function write($filename, $content) {
        $ret = false;
        if (chmod(dirname($filename), 0760)) {
            $ret = file_put_contents($filename, $content);
        }
        return $ret;
    }

    /**
     * Append content to a file
     * @param $filename
     * @param $content
     * @return bool|int
     */
    public static function append($filename, $content) {
        $ret = false;
        if (chmod(dirname($filename), 0760)) {
            $ret = file_put_contents($filename, $content, FILE_APPEND);
        }
        return $ret;
    }

    /**
     * Delete a file
     * @param $filename
     * @return bool
     */
    public static function delete($filename) {
        return unlink($filename);
    }

    /**
     * Clear a folder
     * @param $path
     */
    public static function clearFolder($path) {
        array_map('unlink', glob($path));
    }

    /**
     * Retrieve all files inside a folder
     * @param $dir
     * @param string $pattern
     * @return \RegexIterator|string
     */
    public static function getFileArray($dir, $pattern = '') {
        $ret = '';
        if (is_dir($dir)) {
            $Directory = new RecursiveDirectoryIterator($dir);
            $Iterator = new RecursiveIteratorIterator($Directory);
            $ret = new RegexIterator($Iterator, $pattern, RecursiveRegexIterator::GET_MATCH);
        }
        return $ret;
    }

    /**
     * Browse a directory and return the content
     * @param $dir
     * @param string $prefix
     * @return array
     */
    public static function browseDirectory($dir, $prefix = '') {
        $ret = '';
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != '.' && $file != '..') {
                        $ret[] = $prefix . $file;
                    }
                }
                closedir($dh);
            }
        }
        return $ret;
    }

    /**
     * Download a file
     * @param $filename
     * @param $path
     * @param $mime
     */
    public static function download($filename, $path, $mime) {
        header("Content-disposition: attachment; filename=" . $filename . ".pdf");
        header("Content-type: " . $mime);
        readfile($path);
    }

    /**
     * Determine if a class exists or not
     * @param $className
     * @return bool
     */
    public static function classExists($className) {
        return (class_exists($className) || class_exists(ucfirst($className)));
    }
}