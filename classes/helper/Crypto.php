<?php
/**
 * Crypto.class.php
 * Cryptography class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Crypto {
    /**
     * Encrypt data
     * @param $data
     * @param $publicKeyPath
     * @return string
     * @throws Exception
     */
    public static function encrypt($data, $publicKeyPath) {
        // Compress the data to be sent
        $data = gzencode($data, 9);
        // Get the public Key of the recipient
        $publicKey = openssl_pkey_get_public('file:///' . $publicKeyPath);
        $a_key = openssl_pkey_get_details($publicKey);
        // Encrypt the data in small chunks and then combine and send it.
        $chunkSize = ceil($a_key['bits'] / 8) - 11;
        $output = '';
        while ($data) {
            $chunk = substr($data, 0, $chunkSize);
            $data = substr($data, $chunkSize);
            $encrypted = '';
            if (!openssl_public_encrypt($chunk, $encrypted, $publicKey)) {
                throw new Exception('Error encrypting data');
            }
            $output .= $encrypted;
        }
        openssl_free_key($publicKey);
        // This is the final encrypted data to be sent to the recipient
        return $output;
    }

    /**
     * Decrypt encrypted data
     * @param $data
     * @param $privateKeyPath
     * @return string
     * @throws Exception
     */
    public static function decrypt($data, $privateKeyPath)
    {
        $output = '';
        // Get the private Key
        if (!$privateKey = openssl_pkey_get_private('file:///' . $privateKeyPath)) {
            $a_key = openssl_pkey_get_details($privateKey);
            // Decrypt the data in the small chunks
            $chunkSize = ceil($a_key['bits'] / 8);
            while ($data) {
                $chunk = substr($data, 0, $chunkSize);
                $data = substr($data, $chunkSize);
                $decrypted = '';
                if (!openssl_private_decrypt($chunk, $decrypted, $privateKey)) {
                    throw new Exception('Error decrypting data');
                }
                $output .= $decrypted;
            }
            openssl_free_key($privateKey);
            // Uncompress the unencrypted data.
            $output = gzdecode($output);
        }
        return $output;
    }
}