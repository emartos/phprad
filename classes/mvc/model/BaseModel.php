<?php
/**
 * BaseModel.class.php
 * Base Model
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
abstract class BaseModel {
	/**
	 * Database handler ID array
	 * @var array
	 */
	protected $handlerId;
	/**
	 * Attributes
	 * @var array
	 */
	protected $attributes;

	/**
	 * Class constructor
	 */
	public function __construct() {}

	/**
	 * Initialize the db handler
	 * @param $db
	 */
	protected function initializeDb($db) {
		$params = Parameters::getInstance();
		$this->handlerId[$db] = $params->get('database')[$db]['handler'];
	}

	/**
	 * Get the specified db handler
	 * @param $db
	 * @return mixed
	 */
	protected function getDbHandler($db) {
		return $this->handlerId[$db];
	}

	/**
	 * Assign the values to the attributes array
	 * @param $key
	 * @param $value
	 */
	protected function assign($key, $value) {
		$this->attributes[$key] = $value;
	}

	/**
	 * Getter
	 * @param $key
	 * @return bool
	 */
	public function get($key) {
		return ($this->attributes[$key] !== false) ? $this->attributes[$key] : false;
	}
}