<?php
/**
* IModel.interface.php
* Model interface
* @author Eduardo Martos <emartos@natiboo.es>
* All phpRAD code is released under the GNU General Public License
* See COPYRIGHT.txt and LICENSE.txt
*/
interface IModel {
    /**
     * Load values
     * @param string $params
     * @return mixed
     */
    function load($params = '');

    /**
     * Getter
     * @param $key
     * @return mixed
     */
    function get($key);
}