<?php
/**
 * BaseController.class.php
 * Base Controller
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
abstract class BaseController {
	/**
	 * Arguments
	 * @var array
	 */
	protected $args;
	/**
	 * Controller ID
	 * @var string
	 */
	protected $controllerId;
	/**
	 * Model instance
	 * @var
	 */
	protected $model;
	/**
	 * Controller title
	 * @var string
	 */
	protected $title;
	/**
	 * If true, output is only returned
	 * @var boolean
	 */
	protected $silent;
	/**
	 * List of renderers enabled for this controller
	 * @var array
	 */
	protected $renderers;
	/**
	 * Type of controller
	 * @var string
	 */
	protected $type;
	/**
	 * The view includes header and footer
	 * @var boolean
	 */
	protected $includeHeaderFooter;
	/**
	 * Output to file
	 * @var boolean
	 */
	protected $outputToFile;

	/**
	 * Class constructor
	 * @param $controllerId
	 * @param bool $silent
	 */
	public function __construct($controllerId, $silent = false) {
		$this->controllerId = $controllerId;
		$this->silent = $silent;
		$this->includeHeaderFooter = true;
		$this->outputToFile = false;
		$this->title = 'Default title';
		// Load the model
		$className = $this->controllerId . 'Model';
		if (File::classExists($className)) {
			$this->model = new $className();
		}
	}

	/**
	 * Execute an action defined in the specific controller
	 */
	public function executeAction() {
		$params = Parameters::getInstance();
		$action  = $params->get('action');
		if (method_exists($this, $action)) {
			$this->$action();
		}
	}

	/**
	 * Execute a bundled action and return the rendered content
	 * @param $controllerId
	 * @return mixed
	 * @throws RendererException
	 */
	protected function executeBundledAction($controllerId) {
		// Execute action
		$classname = $controllerId . 'Controller';
		$controller = new $classname($controllerId, true);
		$controller->setBundle();
		$content = $controller->execute();
		// Return rendered content
		try {
			$content = $controller->render($content, true);
		} catch (RendererException $e) {
			throw new RendererException($e->getMessage());
		}
		return $content;
	}

	/**
	 * Set the controller to bundle mode to make it silent
	 */
	private function setBundle() {
		$this->silent = true;
	}

	/**
	 * Render the view
	 * @param $content
	 * @param bool|false $isBundledAction
	 * @return mixed
	 * @throws RendererException
	 */
	public function render($content, $isBundledAction = false) {
		try {
			foreach ($this->renderers as $renderer) {
				$inst = new $renderer($this->controllerId);
				$inst->setIsBundledAction($isBundledAction);
				$inst->setIncludeHeaderFooter($this->includeHeaderFooter);
				$inst->setOutputToFile($this->outputToFile);
				$ret = $inst->render($content);
				if (!$ret) {
					$message = 'Render failed: ' . $renderer;
					$messageBus = MessageBus::getInstance();
					$messageBus->put($message, _LOG_ERROR, true);
				} else if ($renderer == HTML_RENDERER) {
					$content = $ret;
				}
			}
		} catch (RendererException $e) {
			throw new RendererException($e->getMessage());
		}
		// Print output
		if (!$this->silent) {
			print $content;
		}
		return $content;
	}
}