<?php
/**
 * IController.interface.php
 * Controller interface
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
interface IController {
	/**
	 * Execute the action
	 * @return string
	 */
	public function execute();

	/**
	 * Render the view
	 * @param $content
	 * @return mixed
	 */
	public function render($content);
}
