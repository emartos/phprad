<?php
/**
 * FrontController.class.php
 * Front controller
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class FrontController {
	/**
	 * Controller ID
	 * @var string
	 */
	private $controllerId;

	/**
	 * Forward the request to the Synchronizer
	 */
	public function forward() {
		try {
			// Get an instance of the controller
			$controller = $this->checkType();
			// Execute the controller
			$start = microtime(true);
			if (method_exists($controller, 'executeAction')) {
				$controller->executeAction();
			}
			$content = $controller->execute();
			$end = microtime(true);
			$logger = Logger::getInstance();
			$logger->log('Execution of ' . $this->controllerId . ' in ' . Date::getElapsedTime($start, $end), _LOG_NOTICE);
			// Render the view
			$controller->render($content);
		} catch (ExecutionException $e) {
			Error::fatal($e->getMessage());
		} catch (ParametersException $e) {
			Error::fatal($e->getMessage());
		} catch (RendererException $e) {
			Error::fatal($e->getMessage());
		} catch (Exception $e) {
			Error::fatal($e->getMessage());
		}
	}

	/**
	 * Determine if we are executing an action or a bundle
	 * @return mixed
	 * @throws ExecutionException
	 */
	private function checkType() {
		$params = Parameters::getInstance();
		if ($action = $params->get('action')) {
			$this->controllerId = $action;
		} else {
			$this->controllerId = $params->get('bundle');
		}
		$className = $this->controllerId . 'Controller';
		if (!File::classExists($className)) {
			throw new ExecutionException('Controller not found');
		}
		$controller = new $className($this->controllerId);
		return $controller;
	}
}