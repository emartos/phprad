<?php
/**
 * Logger.class.php
 * Very simple logger for PHP
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Logger {
	/**
	 * Filename
	 * @var string
	 */
	private $filename;

	/**
	 * Level for logging messages
	 * @var integer
	 */
	private $maxLevel;

	/**
	 * Private class constructor so nobody else can instance it
	 */
	private function __construct() {}

	/**
	 * Private class clone so nobody else can clone it
	 */
	private function __clone() {}

	/**
	 * Return the standalone instance of the singleton
	 * @param string $filename
	 * @param $maxLevel
	 * @return Config|Logger
	 */
	public static function getInstance($filename = '', $maxLevel = _LOG_ERROR) {
		static $inst = null;
		if ($inst === null) {
			$class = __CLASS__;
			$inst = new $class();
			$inst->filename = $filename;
			$inst->maxLevel = $maxLevel;
		}
		return $inst;
	}

	/**
	 * Translate the log level
	 * @param integer $level
	 * @return string
	 */
	private function translateLevel($level) {
		$ret = '';
		if ($level >= 0 && $level <= _LOG_DEBUG) {
			switch ($level) {
				case _LOG_FATAL:
					$ret = 'FATAL: ';
				break;
				case _LOG_ERROR:
					$ret = 'ERROR: ';
				break;
				case _LOG_WARNING:
					$ret = 'WARNING: ';
				break;
				case _LOG_NOTICE:
					$ret = 'NOTICE: ';
				break;
				case _LOG_DEBUG:
					$ret = 'DEBUG: ';
				break;
			}
		}
		return $ret;
	}

	/**
	 * Log a message
	 * @param $message
	 * @param $level
	 */
	public function log($message, $level = _LOG_ERROR) {
		if ($level <= $this->maxLevel) {
			$line = '['.date('d-m-Y H:i:s').'] '.$this->translateLevel($level).$message . NL;
			File::append($this->filename, $line);
		}
	}
}