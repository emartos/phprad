<?php
/**
 * MessageBus.class.php
 * Message bus to store all the messages of an execution
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class MessageBus {
	/**
	 * Messages array
	 * @var array
	 */
	private $messages;

	/**
	 * Private class constructor so nobody else can instance it
	 */
	private function __construct() {}

	/**
	 * Private class clone so nobody else can clone it
	 */
	private function __clone() {}

	/**
	 * Return the standalone instance of the singleton
	 * @return MessageBus
	 */
	public static function getInstance() {
		static $inst = null;
		if ($inst === null) {
			$class = __CLASS__;
			$inst = new $class();
		}
		return $inst;
	}

	/**
	 * Add a message to the bus
	 * @param $text
	 * @param $type
	 * @param bool $log
	 */
	public function put($text, $type, $log = false) {
		$message = new Message($text, $type);
		$this->messages[] = $message;
		if ($log) {
			$logger = Logger::getInstance();
			$logger->log($text, $type);
		}
	}

	/**
	 * Retrieve the bus content
	 */
	public function getMessages() {
		$ret = '';
		if (!is_null($this->messages)) {
			foreach ($this->messages as $message) {
				$ret .= $message->render();
			}
		}
		return $ret;
	}

	/**
	 * Print the bus content
	 */
	public function printMessages() {
		if (!is_null($this->messages)) {
			foreach ($this->messages as $message) {
				echo $message->render();
			}
		}
	}
}