<?php
/**
 * Message.class.php
 * Class to store a GUI message
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Message {
	/**
	 * Message literal
	 * @var string
	 */
	private $text;

	/**
	 * Type of the message
	 * @var integer
	 */
	private $type;

	/**
	 * Class constructor
	 * @param $text
	 * @param $type
	 */
	public function __construct($text, $type) {
		$this->text = $text;
		$this->type = $type;
	}

	/**
	 * Print the message
	 * @return string
	 */
	public function render() {
		$output = '';
		$errorCodes = unserialize(ERROR_CODES);
		try {
			$renderer = new HtmlRenderer(APP_SAPI, TPL . 'messages/');
			$output = $renderer->render(array('messages' => $errorCodes[$this->type] . ': ' . $this->text));
		} catch (RendererException $e) {
			Error::fatal($e->getMessage());
		}
		return $output;
	}
}