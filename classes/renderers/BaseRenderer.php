<?php
/**
 * Renderer.class.php
 * Abstract renderer
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
abstract class BaseRenderer {
    /**
     * @var string View name
     */
    protected $view;
    /**
     * @var string Base path
     */
    protected $basePath;
    /**
     * @var boolean Show/hide the header and footer
     */
    protected $includeHeaderFooter;
    /**
     * @var boolean If the action is bundled, render header and footer once
     */
    protected $isBundledAction;
    /**
     * @var boolean Save the output to a file
     */
    protected $outputToFile;

    /**
     * Set the view path
     * @param string $view
     * @param string $basePath
     * @param bool|true $isDir
     * @return string
     * @throws RendererException
     */
    protected function getViewPath($view = '', $basePath = '', $isDir = true) {
        if (File::exists($basePath)) {
            $viewPath = $basePath . $view;
        } else if (File::exists(VIEW . $view)) {
            $viewPath = VIEW . $view;
        } else if (File::exists(TPL . $view)) {
            $viewPath = ($isDir) ? TPL . $view : TPL;
        } else {
            // View not found
            throw new RendererException('View not found');
        }
        return $viewPath;
    }

    /**
     * Set the include header/footer mode
     * @param $includeHeaderFooter
     */
    public function setIncludeHeaderFooter($includeHeaderFooter) {
        $this->includeHeaderFooter = $includeHeaderFooter;
    }

    /**
     * Set the bundled mode
     * @param $isBundledAction
     */
    public function setIsBundledAction($isBundledAction) {
        $this->isBundledAction = $isBundledAction;
    }

    /**
     * Set the output mode
     * @param $outputToFile
     */
    public function setOutputToFile($outputToFile) {
        $this->outputToFile = $outputToFile;
    }
}