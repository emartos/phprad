<?php
/**
 * PdfRenderer.class.php
 * Convert HTML content into PDF
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class PdfRenderer extends BaseRenderer implements IRenderer {
	/**
	 * Class constructor
	 * @param $view
	 */
	public function __construct($view) {
		$this->view = $view;
	}

	/**
	 * Save the content into PDF
	 * @param $content
	 * @return bool|mixed
	 */
	public function render($content) {
		$ret = false;
		$tempFile = TEMP . $this->view . '.html';
		if (File::write($tempFile, $content)) {
			$params = Parameters::getInstance();
			$outputFile = VIEWS_OUTPUT . $this->view . '-' . APP_SAPI . '-' . Date::now() . '.pdf';
			$exec = $params->get('pdf_command') . ' ' . $params->get('pdf_command_params') . ' "' .
					$tempFile . '" "' . $outputFile . '"';
			exec($exec, $output, $ret);
			File::delete($tempFile);
			$ret = File::exists($outputFile);
		}
		return $ret;
	}
}