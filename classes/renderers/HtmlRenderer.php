<?php
/**
 * HtmlRenderer.class.php
 * Render content into HTML format
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class HtmlRenderer extends BaseRenderer implements IRenderer {
    /**
     * @var array CSS files
     */
    private $css;
    /**
     * @var array JavaScript files
     */
    private $js;
    /**
     * @var string Name of the HTML header template
     */
    private $htmlHeaderTpl;
    /**
     * @var string Name of the HTML footer template
     */
    private $htmlFooterTpl;
    /**
     * @var string Name of the header template
     */
    private $headerTpl;
    /**
     * @var string Name of the footer template
     */
    private $footerTpl;
    /**
     * @var string Name of the body template
     */
    private $bodyTpl;

    /**
     * Class constructor
     * @param $view
     * @param string $basePath
     */
    public function __construct($view, $basePath = '') {
        // Add the CSS folders
        $this->css[] = array('path' => CSS, 'url' => CSS_URL);
        if (File::exists(VIEW . $view . SPECIFIC_CSS)) {
            $this->css = array(
                'path' => VIEW . $view . SPECIFIC_CSS,
                'url' => VIEW_URL . $view . SPECIFIC_CSS);
        }
        // Add the JavaScript folders
        $this->js[] = array('path' => JS, 'url' => JS_URL);
        if (File::exists(VIEW . $view . SPECIFIC_JS)) {
            $this->css = array(
                'path' => VIEW . $view . SPECIFIC_JS,
                'url' => VIEW_URL . $view . SPECIFIC_JS);
        }
        $params = Parameters::getInstance();
        $this->view = $view;
        $this->basePath = $basePath;
        $this->htmlHeaderTpl = $params->get('html_header_tpl');
        $this->htmlFooterTpl = $params->get('html_footer_tpl');
        $this->headerTpl = $params->get('header_tpl');
        $this->footerTpl = $params->get('footer_tpl');
        $this->bodyTpl = $params->get('body_tpl');
    }

    /**
     * Generate the header/footer
     * @param       $tpl
     * @param array $contentArray
     * @return mixed
     * @throws RendererException
     */
    private function renderPart($tpl, $contentArray = array()) {
        try {
            $viewPath = $this->getViewPath($tpl, $this->basePath, false);
            $loader = new Twig_Loader_Filesystem($viewPath);
            $tplObj = new Twig_Environment($loader);
            $ret = $tplObj->render($tpl, $contentArray);
        } catch (RendererException $e) {
            $ret = '';
        } catch (Exception $e) {
            throw new RendererException($e->getMessage());
        }
        return $ret;
    }

    /**
     * Insert the default header
     * @return mixed
     * @throws RendererException
     */
    private function renderHtmlHeader() {
        $params = Parameters::getInstance();
        $cssFiles = $jsFiles = array();
        foreach ($this->css as $css) {
            $cssFiles = array_merge($cssFiles, File::browseDirectory($css['path'], $css['url']));
        }
        foreach ($this->js as $js) {
            $jsFiles = array_merge($jsFiles, File::browseDirectory($js['path'], $js['url']));
        }
        $content = array(
            'css' => $cssFiles,
            'js' => $jsFiles,
            'nonce' => $params->get('nonce'),
        );
        try {
            $viewPath = $this->getViewPath($this->htmlHeaderTpl, $this->basePath, false);
            $loader = new Twig_Loader_Filesystem($viewPath);
            $tplObj = new Twig_Environment($loader);
            $ret = $tplObj->render($this->htmlHeaderTpl, $content);
        } catch (RendererException $e) {
            throw new RendererException($e->getMessage());
        } catch (Exception $e) {
            throw new RendererException($e->getMessage());
        }
        return $ret;
    }

    /**
     * Insert the default footer
     * @return mixed
     * @throws RendererException
     */
    private function renderHtmlFooter() {
        try {
            $viewPath = $this->getViewPath($this->htmlFooterTpl, $this->basePath, false);
            $loader = new Twig_Loader_Filesystem($viewPath);
            $tplObj = new Twig_Environment($loader);
            $ret = $tplObj->render($this->htmlFooterTpl);
        } catch (RendererException $e) {
            throw new RendererException($e->getMessage());
        } catch (Exception $e) {
            throw new RendererException($e->getMessage());
        }
        return $ret;
    }

    /**
     * Render content into HTML format
     * @param $content
     * @return bool|string
     * @throws RendererException
     */
    public function render($content) {
        Twig_Autoloader::register();
        $htmlHeader = $htmlFooter = $header = $footer = '';
        // Twig needs an array as an input parameter
        if (is_null($content)) {
            $content = array();
        }
        try {
            $viewPath = $this->getViewPath($this->view, $this->basePath);
            $loader = new Twig_Loader_Filesystem($viewPath);
            $tplObj = new Twig_Environment($loader);
            $body = $tplObj->render($this->bodyTpl, $content);
        } catch (Exception $e) {
            throw new RendererException($e->getMessage());
        }
        if ($this->includeHeaderFooter) {
            if (APP_SAPI == HTTP && !$this->isBundledAction) {
                $htmlHeader = $this->renderHtmlHeader();
                $htmlFooter = $this->renderHtmlFooter();
            }
            $header = $this->renderPart($this->headerTpl, $content);
            $footer = $this->renderPart($this->footerTpl, $content);
        }
        $ret = $htmlHeader . $header. $body . $footer . $htmlFooter;
        if ($this->outputToFile) {
            $outputFile = VIEWS_OUTPUT . $this->view . '-' . APP_SAPI . '-' . Date::now() . '.html';
            if (!File::write($outputFile, $ret)) {
                $ret = false;
            }
        }
        return $ret;
    }
}