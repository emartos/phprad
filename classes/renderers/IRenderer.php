<?php
/**
 * IRenderer.interface.php
 * Renderer interface
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
interface IRenderer {
	/**
	 * Render the content
	 * @param $content
	 * @return mixed
	 */
	public function render($content);
}