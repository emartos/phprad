<?php
/**
 * IEnvironment.interface.php
 * Environment interface
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
interface IEnvironment {
	/**
	 * Parse CLI arguments
	 */
	public function parseParameters();
}
