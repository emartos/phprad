<?php
/**
 * Cli.class.php
 * Command line environmental handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Cli extends Environment implements IEnvironment {
	/**
	 * Class constructor
	 * @param $argv
	 */
	public function __construct($argv) {
		$path = pathinfo((realpath($argv[0])));
		// Absolute path to the application
		Constants::set('APP_ROOT', $path['dirname'] . '/');
		// URL of the application
		Constants::set('APP_URL', APP_ROOT);
		// SAPI mode
		Constants::set('APP_SAPI', CLI);
		// New line
		Constants::set('NL', "\n");
	}

	/**
	 * Parse CLI arguments
	 */
	public function parseParameters() {
		$params = Parameters::getInstance();
		$argv = $GLOBALS['argv'];
		array_shift($argv);
		$argc = count($argv);
		for ($i = 0; $i < $argc; $i++) {
			$token = $argv[$i];
			if (strpos($token, '=') !== false) {
				$arg = explode('=', $token);
				$params->set($arg[0], $arg[1]);
			}
		}
	}
}