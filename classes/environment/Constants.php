<?php
/**
 * Constants.class.php
 * Constants handler
 * 		Why constants? Constants are features usually disregarded because they are potentially dangerous. But used carefully, constants are very useful.
 * 		Imagine this situation: You need to define an application path root that you use everywhere. You could have this stored in a singleton:
 * 			$params = Parameters::getInstance();
 * 			$path = $params->get('APP_ROOT') . 'whatever';
 *		In the other hand, we had constants, which are much more simple:
 *			$path = APP_ROOT . 'whatever';
 *		And you don't have to worry about scope because constants cannot be reassigned
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Constants {
	/**
	 * Set a constant
	 * @param string $key
	 * @param string $value
	 */
	public static function set($key, $value) {
		if (!defined($key)) {
			define($key, $value);
		}
	}
}
