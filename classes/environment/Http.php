<?php
/**
 * Http.class.php
 * HTTP environmental handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Http extends Environment implements IEnvironment {
	/**
	 * Class constructor
	 */
	public function __construct() {
		// Absolute path to the application
		Constants::set('APP_ROOT', dirname($_SERVER['SCRIPT_FILENAME']) . '/');
		// URL of the application
		$phpSelf = pathinfo($_SERVER['PHP_SELF']);
		$context = $phpSelf['dirname'];
		Constants::set('APP_URL', 'http://'.$_SERVER['HTTP_HOST'] . $context . '/');
		Constants::set('APP_PORT', intval($_SERVER['SERVER_PORT']) != 80 ? ':' . $_SERVER['SERVER_PORT'] : '');
		// SAPI mode
		Constants::set('APP_SAPI', HTTP);
		// New line
		Constants::set('NL', '<br />');
	}

	/**
	 * Parse GET parameters
	 */
	function parseParameters() {
		$params = Parameters::getInstance();
		foreach ($_REQUEST as $key => $value) {
			$params->set($key, $value);
		}
		foreach ($_FILES as $key => $value) {
			$params->set($key, $value['tmp_name']);
		}
	}
}