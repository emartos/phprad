<?php
/**
 * Environment.class.php
 * Environment parent class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Environment {
	/**
	 * Class constructor
	 */
	public function __construct() {
		// Environment
		Constants::set('CLI', 'cli');
		Constants::set('HTTP', 'http');
		// Type of controller
		Constants::set('C_CONTROLLER', 'controller');
		Constants::set('C_BUNDLE', 'bundle');
		// Renderers
		Constants::set('HTML_RENDERER', 'HtmlRenderer');
		Constants::set('PDF_RENDERER', 'PdfRenderer');
		// Message/Logger
		Constants::set('_LOG_FATAL', 0);
		Constants::set('_LOG_ERROR', 1);
		Constants::set('_LOG_WARNING', 2);
		Constants::set('_LOG_NOTICE', 3);
		Constants::set('_LOG_DEBUG', 4);
		Constants::set('ERROR_CODES', serialize(array(
			_LOG_FATAL => 'FATAL',
			_LOG_ERROR => 'ERROR',
			_LOG_WARNING => 'WARNING',
			_LOG_NOTICE => 'NOTICE',
			_LOG_DEBUG => 'DEBUG',
		)));
	}

	/**
	 * Check the application parameters
	 * @return bool
	 * @throws ParametersException
	 */
	private function checkParameters() {
		$params = Parameters::getInstance();
		// You can execute actions or bundles
		if (!$params->get('action') && !$params->get('bundle')) {
			$message[CLI] = 'You must specify an action or a bundle: php -f index.php action|bundle=[action id]' . NL . 'e.g. php -f index.php action=create' . NL . 'php -f index.php bundle=generate-site';
			$message[HTTP] = 'You must specify an action or a bundle: http://APPROOT/?action|bundle=action_id' . NL . 'e.g. http://APPROOT/?action=Csv' . NL . 'http://APPROOT/?bundle=Csv';
			throw new ParametersException($message[APP_SAPI]);
		}
		return true;
	}

	/**
	 * Detect the execution environment
	 * @return bool
	 */
	public function detect() {
		$params = Parameters::getInstance();
		$allowedEnvironments = $params->get('environment')['allowed_environments'];
		$sapi = php_sapi_name();
		foreach ($allowedEnvironments as $env) {
			if (strpos($sapi, $env) !== false) {
				$envId = $params->get('environment')[$env]['handler'];
				return $envId;
			}
		}
		return false;
	}

	/**
	 * Set the environment
	 * @throws ParametersException
	 */
	public function set() {
		$params = Parameters::getInstance();
		// Set timezone
		Date::setTimezone($params->get('environment')['timezone']);
		// Paths
		Constants::set('VIEW', APP_ROOT . $params->get('view') . APP_SAPI . '/');
		Constants::set('VIEW_URL', APP_URL . $params->get('view') . APP_SAPI . '/');
		Constants::set('TPL', APP_ROOT . $params->get('templates'));
		Constants::set('LOG', APP_ROOT . $params->get('log'));
		Constants::set('LOG_FILE', $params->get('log_file'));
		Constants::set('VIEWS_OUTPUT', APP_ROOT . $params->get('views_output'));
		Constants::set('TEMP', APP_ROOT . $params->get('temp'));
		Constants::set('CSS', APP_ROOT . $params->get('css'));
		Constants::set('JS', APP_ROOT . $params->get('js'));
		Constants::set('CSS_URL', APP_URL . $params->get('css'));
		Constants::set('JS_URL', APP_URL . $params->get('js'));
		Constants::set('SPECIFIC_CSS', $params->get('specific_css'));
		Constants::set('SPECIFIC_JS', $params->get('specific_js'));
		Constants::set('IMG_PATH', APP_ROOT . $params->get('img'));
		Constants::set('IMG_URL', APP_URL . $params->get('img'));
		// Initialize logger
		Logger::getInstance(LOG . LOG_FILE, _LOG_NOTICE);
		// Parse parameters
		$this->parseParameters();
		// Check parameters
		$this->checkParameters();
		// Set the security nonce
		$this->setSecurityNonce();
	}

	/**
	 * Set the security nonce
	 */
	private function setSecurityNonce() {
		$nonce  = chr(mt_rand(97, 122)) . substr(md5(time()), 1);
		$params = Parameters::getInstance();
		$params->set('nonce', $nonce);
	}
}