<?php
/**
 * RedisHandler.class.php
 * Redis DBMS handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class RedisHandler extends DbHandler implements IDbHandler {
	/**
	 * Class constructor
	 */
	public function __construct() {
		parent::__construct('redis');
	}

	/**
	 * Try to connect to the database
	 * Return true if success, otherwise return false
	 * @return boolean
	 */
	public function connect() {
		$detail = '';
		Predis\Autoloader::register();
		try {
			$this->connId = new Predis\Client(array(
				'scheme' => 'tcp',
				'host' => $this->host,
				'port' => $this->port,
				'password' => $this->password,
				'timeout' => $this->timeout,
				'database' => $this->dbName,
				'read_write_timeout' => -1
			));
			$ret = true;
		} catch (Predis\Connection\ConnectionException $e) {
			$detail = $e->getMessage();
			$ret = false;
		} catch (Exception $e) {
			$detail = $e->getMessage();
			$ret = false;
		}
		if (!$ret) {
			$message = 'Redis connection error: ' . $detail;
			$messageBus = MessageBus::getInstance();
			$messageBus->put($message, _LOG_FATAL, true);
		}
		return $ret;
	}

	/**
	 * Execute a query to build an array of entities
	 * @param $query
	 * @return bool
	 * @throws DbConnectionException
	 */
	public function query($query) {
		$detail = '';
		if ($this->connect()) {
			$ret = true;
			$attempt = 0;
			try {
				while (!$ret && $attempt < $this->attempts) {
					$ret = $this->connId->get($query);
					++$attempt;
				}
			} catch (Predis\Connection\ConnectionException $e) {
				$detail = $e->getMessage();
				$ret = false;
			} catch (Exception $e) {
				$detail = $e->getMessage();
				$ret = false;
			}
		} else {
			$detail = 'attempts exhausted';
			$ret = false;
		}
		if (!$ret) {
			$message = 'Redis connection error: ' . $detail;
			throw new DbConnectionException($message);
		}
		return $ret;
	}
}