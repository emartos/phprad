<?php
/**
 * DbHandler.class.php
 * Handle a database back-end
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class DbHandler {
	/**
	 * Configuration key
	 * @var string
	 */
	protected $key;

	/**
	 * Database hostname or IP
	 * @var string
	 */
	protected $host;

	/**
	 * Database port
	 * @var string
	 */
	protected $port;

	/**
	 * Database name
	 * @var string
	 */
	protected $dbName;

	/**
	 * Database username
	 * @var string
	 */
	protected $user;

	/**
	 * Database password
	 * @var string
	 */
	protected $password;

	/**
	 * Attempts to reconnect
	 * @var integer
	 */
	protected $attempts;

	/**
	 * Timeout
	 * @var integer
	 */
	protected $timeout;

	/**
	 * Connection identifier
	 * @var resource
	 */
	protected $connId;

	/**
	 * Class constructor
	 * @param $db
	 */
	public function __construct($db) {
		$params = Parameters::getInstance();
		$this->key = $db;
		$this->host = $params->get('database')[$db]['host'];
		$this->port = $params->get('database')[$db]['port'];
		$this->dbName = $params->get('database')[$db]['db_name'];
		$this->user = $params->get('database')[$db]['user'];
		$this->password = $params->get('database')[$db]['password'];
		$this->attempts = $params->get('database')[$db]['attempts'];
		$this->timeout = $params->get('database')[$db]['timeout'];
	}
}