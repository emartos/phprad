<?php
/**
 * Entity.class.php
 * Entity ORM
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Entity {
	/**
	 * Array of dynamic attributes of the class
	 * @var array
	 */
	var $attributes;

	/**
	 * Class constructor
	 */
	public function __construct() {}

	/**
	 * @param $name
	 * @param $value
	 */
	public function setAttribute($name, $value) {
		$this->attributes[$name] = $value;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function getAttribute($name) {
		return $this->attributes[$name];
	}

	/**
	 * @return array
	 */
	public function getAttributes() {
		return $this->attributes;
	}

	/**
	 * @param $rowset
	 */
	public function setRowset($rowset) {
		foreach ($rowset as $key => $value) {
			$this->setAttribute($key, $value);
		}
	}
}