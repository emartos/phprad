<?php
/**
 * IDbHandler.interface.php
 * DB handler interface
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
interface IDbHandler {
	/**
	 * @return mixed
	 */
	public function connect();

	/**
	 * @param $query
	 * @return mixed
	 */
	public function query($query);
}