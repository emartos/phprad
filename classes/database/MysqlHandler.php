<?php
/**
 * MysqlHandler.class.php
 * MySQL DBMS handler
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class MysqlHandler extends DbHandler implements IDbHandler {
	/**
	 * Class constructor
	 */
	public function __construct() {
		parent::__construct('mysql');
	}

	/**
	 * Try to connect to the database
	 * Return true if success, otherwise return false
	 * @return boolean
	 */
	public function connect() {
		$ret = false;
		if ($this->connId = new mysqli($this->host, $this->user, $this->password)) {
			$ret = $this->connId->select_db($this->dbName);
		}
		return $ret;
	}

	/**
	 * Try to close the open connection with the database
	 * Return true if success, otherwise return false
	 * @return boolean
	 */
	private function close() {
		return $this->connId->close();
	}

	/**
	 * Execute a query to build an array of entities
	 * Return an array of entities if success and there is any row, otherwise return false
	 * @param $query
	 * @return array|bool
	 */
	public function query($query) {
		$ret = false;
		if (!$this->connect()) {
			$message = 'MySQL connection error: ' . $this->connId->error;
			$messageBus = MessageBus::getInstance();
			$messageBus->put($message, _LOG_FATAL, true);
		} else {
			// Check the incorporation date
			if ($resultset = $this->connId->query($query)) {
				if ($resultset->num_rows > 0) {
					$entityArray = array();
					while ($row = $resultset->fetch_assoc()) {
						$entity = new Entity();
						$entity->setRowset($row);
						$entityArray[] = $entity->getAttributes();
					}
					$ret = $entityArray;
				} else {
					$message = 'There is no records matching your criteria';
					$messageBus = MessageBus::getInstance();
					$messageBus->put($message, LOG_NOTICE);
				}
			} else {
				$message = 'There is no records matching your criteria';
				$messageBus = MessageBus::getInstance();
				$messageBus->put($message, LOG_NOTICE);
			}
			if (!$this->close()) {
				$message = 'Error closing MySQL connection: ' . $this->connId->error;
				$messageBus = MessageBus::getInstance();
				$messageBus->put($message, _LOG_FATAL, true);
			}
		}
		return $ret;
	}

	/**
	 * Execute a query and return the number of results
	 * @param $query
	 * @return int
	 */
	public function exists($query) {
		$ret = 0;
		if (!$this->connect()) {
			$message = 'MySQL connection error: ' . $this->connId->error;
			$messageBus = MessageBus::getInstance();
			$messageBus->put($message, _LOG_FATAL, true);
		} else {
			if ($resultset = $this->connId->query($query)) {
				$ret = $resultset->num_rows;
			}
			if (!$this->close()) {
				$message = 'Error closing MySQL connection: ' . $this->connId->error;
				$messageBus = MessageBus::getInstance();
				$messageBus->put($message, _LOG_FATAL, true);
			}
		}
		return $ret;
	}

	/**
	 * Execute a query
	 * @param $query
	 * @return bool|resource
	 */
	public function execute($query) {
		$ret = false;
		if (!$this->connect()) {
			$message = 'MySQL connection error: ' . $this->connId->error;
			$messageBus = MessageBus::getInstance();
			$messageBus->put($message, _LOG_FATAL, true);
		} else {
			$ret = $this->connId->query($query);
			if (!$this->close()) {
				$message = 'Error closing MySQL connection: ' . $this->connId->error;
				$messageBus = MessageBus::getInstance();
				$messageBus->put($message, _LOG_FATAL, true);
			}
		}
		return $ret;
	}

	/**
	 * Execute a script
	 * @param $filename
	 * @return bool
	 */
	public function executeScript($filename) {
		$ret = false;
		if (!$this->connect()) {
			$message = 'MySQL connection error: ' . $this->connId->error;
			$messageBus = MessageBus::getInstance();
			$messageBus->put($message, _LOG_FATAL, true);
		} else {
			if ($script = File::read($filename)) {
				$script = explode(';', $script);
				$ret = true;
				foreach($script as $line) {
					$line = trim($line);
					if (!empty($line) && !is_null($line)) {
						$ret = $ret && $this->connId->query($line);
					}
				}
			}
			if (!$this->close()) {
				$message = 'Error closing MySQL connection: ' . $this->connId->error;
				$messageBus = MessageBus::getInstance();
				$messageBus->put($message, _LOG_FATAL, true);
			}
		}
		return $ret;
	}
}