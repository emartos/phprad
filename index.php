<?php
/**
 * App.class.php
 * Application launcher
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class App {
	/**
	 * Arguments
	 * @var array
	 */
	private $argv;

	/**
	 * Class constructor
	 * @param $argv
	 */
	public function __construct($argv) {
		$this->argv = $argv;
	}

    private function loadClasses() {
        require('vendor/autoload.php');
        require('Autoloader.php');
    }

	/**
	 * Initialize the environment
	 */
	private function initializeEnvironment() {
		// Detect the execution environment
		$baseEnvironment = new Environment();
		$envId = $baseEnvironment->detect();
		if ($envId) {
			$environment = new $envId($this->argv);
		} else {
			die('Environment not allowed');
		}
		// Error initializing the environment
		try {
			$environment->set();
		} catch (ParametersException $e) {
			Error::fatal($e->getMessage());
		}
	}

	/**
	 * Launch the front controller
	 */
	private function launchFrontController() {
		// Forward the request through the Front Controller and get the output messages
		$frontController = new FrontController();
		$frontController->forward();
	}

	/**
	 * Launch the application
	 */
	public function start() {
		$this->loadClasses();
		$this->initializeEnvironment();
		$this->launchFrontController();
	}
}
// Launch the application
$args = isset($argv) ? $argv : array();
$app = new App($args);
$app->start();