<?php
/**
 * Autoloader.class.php
 * Autoloader class
 * @author Eduardo Martos <emartos@natiboo.es>
 * All phpRAD code is released under the GNU General Public License
 * See COPYRIGHT.txt and LICENSE.txt
 */
class Autoloader {
    /**
     * List of folders where classes may be stored
     * @var array
     */
    protected static $folders = array(
        'classes/database/',
        'classes/environment/',
        'classes/exception/',
        'classes/helper/',
        'classes/logger/',
        'classes/messages/',
        'classes/renderers/',
        'classes/mvc/controller/',
        'classes/mvc/model/',
        'src/controller/',
        'src/controller/actions/',
        'src/controller/bundles/',
        'src/controller/ext/',
        'src/model/',
        'src/model/ext/',
    );

    /**
     * Load the class attending to the folders waterfall
     * @param string $class
     */
    public static function loader($class) {
        $appRoot = defined('APP_ROOT') ? APP_ROOT : __DIR__ . '/';
        $ext = '.php';
        foreach (self::$folders as $folder) {
            if ($ret = is_readable($appRoot . $folder . $class . $ext)) {
                require_once($appRoot . $folder . $class . $ext);
                break;
            }
        }
    }
}
spl_autoload_register('Autoloader::loader');